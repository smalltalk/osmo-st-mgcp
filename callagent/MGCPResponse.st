"
 (C) 2011 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: MGCPResponse [
    | code transaction params sdp responseString |

    <category: 'OsmoMGCP-Response'>
    <comment: 'I provide a nicer way to look at responses'>

    MGCPResponse class >> fromDict: aDict [
        | responseString |
        <category: 'creation'>

        responseString := aDict first at: 5.
        responseString ifNotNil: [responseString := responseString second].
        ^ self new
            initialize;
            responseCode: aDict first first;
            responseString: responseString;
            transaction: aDict first third;
            addParamsFromDict: aDict second;
            addSDPFromDict: aDict third;
            yourself
    ]

    MGCPResponse class >> sdpFromDict: aDict [
        | str |

        aDict isNil ifTrue: [
            ^nil
        ].

        str := WriteStream on: (String new).
        ^String streamContents: [:stream |
            aDict second do: [:each |
                stream
                    nextPutAll: each first;
                    cr; nl.]]
    ]

    initialize [
        <category: 'creation'>
        params := Dictionary new.
    ]

    responseCode: aCode [
        <category: 'creation'>
        code := aCode asInteger
    ]

    responseString: aString [
        <category: 'creation'>
        responseString := aString
    ]

    responseString [
        ^responseString
    ]

    transaction: aTrans [
        <category: 'creation'>
        self transactionId: aTrans
    ]

    transactionId: aTrans [
        <category: 'creation'>
        transaction := aTrans.
    ]

    addParamsFromDict: aList [
        <category: 'creation'>

        aList do: [:each |
            params at: each first first put: each first fourth].
    ]

    addSDPFromDict: aDict [
        <category: 'creation'>
        sdp := self class sdpFromDict: aDict.
    ]

    transactionId [
        <category: 'accessing'>
        ^ transaction
    ]

    code [
        <category: 'accessing'>
        ^ code
    ]

    isSuccess [
        <category: 'accessing'>
        ^ code >= 200 and: [code < 300].
    ]

    sdp [
        <category: 'accessing'>
        ^ sdp
    ]

    parameterAt: aKey ifAbsent: aBlock [
        ^ params at: aKey ifAbsent: aBlock.
    ]

    asDatagram [
        ^String streamContents: [:stream |
            stream
                nextPutAll: code asString;
                nextPutAll: ' ';
                nextPutAll: transaction asString.

            responseString ifNotNil: [
                stream
                    nextPutAll: ' ';
                    nextPutAll: responseString].

            stream cr; nl.

            params associationsDo: [:assoc |
                stream
                    nextPutAll: assoc key;
                    nextPutAll: ': ';
                    nextPutAll: assoc value;
                    cr; nl].

            sdp ifNotNil: [
                stream
                    cr; nl;
                    nextPutAll: sdp]]
    ]
]
