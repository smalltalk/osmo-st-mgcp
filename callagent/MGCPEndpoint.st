"
 (C) 2010-2011 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: MGCPEndpoint [
    | nr trunk state callid connid sdp|
    <category: 'OsmoMGCP-Callagent'>
    <comment: 'I am one endpoint. I have a state...'>

    MGCPEndpoint class >> stateUnused [ <category: 'states'> ^ #unused ]
    MGCPEndpoint class >> stateReserved [ <category: 'states'> ^ #reserved ]
    MGCPEndpoint class >> stateUsed [ <category: 'states'> ^ #used ]
    MGCPEndpoint class >> stateBlocked [ <category: 'states'> ^ #blocked ]

    MGCPEndpoint class >> initWith: aNr trunk: aTrunk [
        ^ self new
            instVarNamed: #nr put: aNr;
            instVarNamed: #trunk put: aTrunk;
            instVarNamed: #state put: self stateUnused;
            yourself
    ]

    endpointName [
        <category: 'names'>
        ^ trunk endpointName: nr.
    ]

    endpointNumber [
        <category: 'accessing'>
        ^ nr
    ]

    multiplex [
        <category: 'names'>
        ^ trunk multiplexFor: nr.
    ]

    timeslot [
        <category: 'names'>
        ^ trunk timeslotFor: nr.
    ]

    trunk [
        <category: 'private'>
        ^ trunk
    ]

    state [
        <category: 'state'>
        ^ state
    ]

    isBlocked [
        <category: 'state'>
        ^ state = self class stateBlocked.
    ]

    isReserved [
        <category: 'state'>
        ^ state = self class stateReserved.
    ]

    isUnused [
        <category: 'state'>
        ^ state = self class stateUnused.
    ]

    isUsed [
        <category: 'state'>
        ^ state = self class stateUsed.
    ]

    requireState: aState [
        <category: 'allocation'>
        state = aState ifFalse: [
            ^ self error: ('MGCPEndpoint(<1p>) not <2p>.'
                    expandMacrosWithArguments: {self endpointName. aState}).
        ].
    ]

    reserve [
        <category: 'allocation'>
        self requireState: self class stateUnused.
        state := self class stateReserved.
    ]

    used [
        <category: 'allocation'>
        self requireState: self class stateReserved.
        state := self class stateUsed.
    ]

    free [
        <category: 'allocation'>
        self requireState: self class stateUsed.
        state := self class stateUnused.
        sdp := nil.
        callid := nil.
        connid := nil.
    ]

    tryBlock [
        <category: 'allocation'>
        state = self class stateUnused ifTrue: [
            state := self class stateBlocked.
            ^ true
        ].

        ^ false
    ]

    unblock [
        <category: 'allocation'>
        self requireState: self class stateBlocked.
        state := self class stateUnused.
    ]

    sdp [
        <category: 'sdp'>
        ^ sdp
    ]

    sdp: aSdp [
        <category: 'sdp'>
        self requireState: self class stateUsed.
        sdp := aSdp.
    ]

    callId [
        <category: 'callid'>
        ^ callid
    ]

    callId: aCallId [
        <category: 'callid'>
        self requireState: self class stateReserved.
        callid := aCallId.
    ]

    clearCallId [
        <category: 'callid'>
        self requireState: self class stateUsed.
        callid := nil. 
    ]

    connId [
        <category: 'connid'>
        ^ connid
    ]

    connId: aConnId [
        self requireState: self class stateUsed.
        connid := aConnId.
    ]
]
