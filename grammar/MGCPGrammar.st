"
 (C) 2010-2011 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

PP.PPCompositeParser subclass: MGCPGrammar [
    | MGCPMessage EOL One_WSP MGCPCommandLine MGCPVerb transaction_id endpointName MGCPversion MGCPParameter MGCPCommand ParameterValue SDPRecord SDPLine SDPinformation MGCPResponseLine responseCode responseString packageName wordParser |
    <category: 'OsmoMGCP-Core'>
    <comment: 'I am a the Grammar of the Media Gateway Control Protocol'>

    start [
        <category: 'accessing'>
        ^ MGCPMessage end
    ]

    EOL [
        <category: 'grammar-common'>
        ^ (Character cr asParser, Character lf asParser) /
           Character lf asParser
    ]

    One_WSP [
        <category: 'grammar-common'>
        ^ #blank asParser plus
    ]

    MGCPMessage [
        <category: 'grammar-common'>
        ^ MGCPCommand / self MGCPResponse
    ]

    MGCPCommandLine [
        <category: 'grammar-cmd'>
        ^ self MGCPVerb,
          self One_WSP,
          self transaction_id,
          self One_WSP,
          self endpointName,
          self One_WSP,
          self MGCPversion,
          self EOL
    ]

    MGCPVerb [
        <category: 'grammar-cmd'>
        ^ 'EPCF' asParser /
          'CRCX' asParser /
          'MDCX' asParser /
          'DLCX' asParser /
          'RQNT' asParser /
          'NTFY' asParser /
          'AUEP' asParser /
          'AUCX' asParser /
          'RSIP' asParser
    ]

    transaction_id [
        <category: 'grammar-cmd'>
        "Add Osmocom extension that starts with 'nat-'"
        ^ ((#digit asParser) min: 1 max: 9) flatten /
          ('nat-' asParser, ((#digit asParser) min: 1 max: 9) plus) flatten
    ]

    endpointName [
        <category: 'grammar-cmd'>
        "simplified version"
        ^ #word asParser star flatten, $@ asParser, #word asParser star flatten
    ]

    MGCPversion [
        <category: 'grammar-cmd'>
        "skipping the optional profilename for now"
        ^ 'MGCP' asParser, One_WSP, #digit asParser, $. asParser, #digit asParser
    ]

    MGCPCommand [
        <category: 'grammar-cmd'>
        ^ MGCPCommandLine, MGCPParameter star, SDPRecord optional
    ]

    MGCPParameter [
        <category: 'grammar-cmd'>
        ^ ParameterValue, EOL
    ]

    wordParser [
        ^ (#word asParser / #punctuation asParser / ' ' asParser) star flatten
    ]

    ParameterValue [
        "This code used to check for legal variable names but the way
        it was done created a lot of parsers and choice. Norbert has
        pointed me to use >=> (or >< in GST) to make the keyword check
        in the parser. One example is >>#failOnReservedWords: inside
        the ASN1 code."
        ^ (#word asParser / $- asParser) plus flatten, $: asParser,
            #blank asParser plus, wordParser
    ]

    MGCPResponse [
    "
     MGCPResponse = MGCPResponseLine 0*(MGCPParameter) *2(EOL *SDPinformation)
     The *2 appears to be weird
    "
        ^ MGCPResponseLine,
          MGCPParameter star, 
          SDPRecord optional
    ]

    responseCode [
        <category: 'response'>
        ^ (#digit asParser, #digit asParser, #digit asParser) flatten
    ]

    packageName [
        "Not Complete yet"
        "packageName = 1*(ALPHA / DIGIT / HYPHEN) ; Hyphen neither first or last"
        <category: 'response'>
        ^ #letter asParser plus
    ]

    responseString [
        <category: 'response'>
        ^ #letter asParser plus flatten
    ]

    MGCPResponseLine [
        ^ responseCode,
          self One_WSP,
          transaction_id,
          (self One_WSP, '/' asParser, packageName) optional,
          (self One_WSP, responseString) optional,
          EOL
    ]

    SDPRecord [
        ^ EOL, SDPinformation
    ]

    SDPinformation [
        ^ (SDPLine, EOL) plus
    ]

    SDPLine [
        ^wordParser
    ]
]

Eval [
    MGCPGrammar initialize.
]
