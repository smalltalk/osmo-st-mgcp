"
 (C) 2010-2011 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

PP.PPCompositeParserTest subclass: MGCPGrammarTest [
    <comment: 'I test some parts of the grammar'>
    <category: 'OsmoMGCP-Core-Tests'>

    MGCPGrammarTest class >> packageNamesUnderTest [
        <category: 'accessing'>

        ^#('MGCPGrammar')
    ]

    parserClass [
        <category: 'accessing'>

        ^MGCPGrammar
    ]

    testCommand [
        <category: 'test-command'>

        | res nl |

        nl := Character cr asString, Character nl asString.

        res := self parse: 'AUEP 23444 13@mgw MGCP 1.0',
                        nl,
                        'K: 3445', nl,
                        'I: 233434', nl,
                        'L: p:20, a:GSM-EFR, nt:IN', nl,
                        'M: recvonly', nl,
                        nl,
                        'v=0', nl,
                        'o=- 258696477 0 IN IP4 172.16.1.107', nl,
                        's=-', nl,
                        'c=IN IP4 172.16.1.107', nl,
                        't=0 0', nl,
                        'm=audio 6666 RTP/AVP 127', nl,
                        'a=rtpmap:127 GSM-EFR/8000/1', nl,
                        'a=ptime:20', nl,
                        'a=recvonly', nl,
                        'm=image 4402 udptl t38', nl,
                        'a=T38FaxVersion:0', nl,
                        'a=T38MaxBitRate:14400', nl.
        self assert: res size equals: 10.
        self assert: (res at: 1) equals: 'AUEP'.
        self assert: (res at: 3) equals: '23444'.
        self assert: (res at: 5) equals: #('13' $@ 'mgw').
    ]

    testReply [
        | res nl |

        nl := Character cr asString, Character nl asString.

        res := self parse: '200 123456 OK',
                    nl.

        self assert: res size equals: 3.
        self assert: res first first equals: '200'.
        self assert: res first third equals: '123456'.
    ]

    testReplyByte [
        | res inp |

        inp := #(50 48 48 32 52 51 51 52 49 52 54 53 54 32 79 75 13 10) asByteArray.
        res := self parse: inp asString.
        self assert: res size equals: 3.
        self assert: res first first equals: '200'.
        self assert: res first third equals: '433414656'.
    ]

    testDlcxResponse [
        | res inp |

        inp := #[50 53 48 32 54 56 51 52 53 53 50 52 52 32 79 75 13 10 80 58 32 80 83 61 49 54 57 44 32 79 83 61 55 54 48 53 44 32 80 82 61 48 44 32 79 82 61 48 44 32 80 76 61 48 44 32 74 73 61 48 13 10 88 45 79 115 109 111 45 67 80 58 32 69 67 32 84 73 83 61 48 44 32 84 79 83 61 48 44 32 84 73 82 61 48 44 32 84 79 82 61 48 13 10].

        res := self parse: inp asString.
    ]
]
