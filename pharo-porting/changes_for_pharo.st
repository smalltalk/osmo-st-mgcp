MGCPDSTrunk extend [
    endpointName: aNr [
        ^'ds/e1-<1p>/<2p>@mgw'
            expandMacrosWithArguments: {trunk. aNr}
    ]
]

MGCPVirtualTrunk extend [
    endpointName: aNr [
        ^'<1s>@mgw' expandMacrosWith: (aNr radix: 16) asLowercase.
    ]
]

MGCPGrammar extend [
    MGCPCommand [
        ^self MGCPCommandLine , MGCPParameter star , SDPRecord optional
    ]
]

MGCPCallAgent extend [
    generateTransactionId [
        1 to: 100 do: [:each |
            | ran |
            ran := (OsmoSecureRandom rand: 4) asInteger bitAnd: 536870911.
            (self transactionIdIsUsed: ran) ifFalse: [
                ^ran]].
        ^self error: 'No free transaction ID.'
    ]
]
