
GST_PACKAGE = gst-package
GST_CONVERT = gst-convert

CONVERT_RULES = -r'Osmo.LogManager->LogManager' \
		-r'Osmo.LogArea->LogArea'  \
		-r'Osmo.LogLevel->LogLevel' \
                -r'Osmo.TimerScheduler->TimerScheduler' \
		-r'Osmo.OsmoUDPSocket->OsmoUDPSocket' \
                -r'Sockets.StreamSocket->SocketStream' \
		-r'Osmo.SIPParser->SIPParser' \
		-r'DateTime->DateAndTime' \
		-r'Character nl->Character lf' \
		-r'(Duration milliseconds: ``@args1) -> (Duration milliSeconds: ``@args1)' \
		-r'PP.PPParser->PPParser' \
		-r'PP.PPCompositeParser->PPCompositeParser' \
		-r'PP.PPCompositeParserTest->PPCompositeParserTest' \
		-r'PP.PPPredicateObjectParser->PPPredicateObjectParser' \
		-r'PP.PPCharSetPredicate->PPCharSetPredicate' \
		-r'Osmo.MessageBuffer->MessageBuffer' \
		-r'SystemExceptions.NotFound->NotFound' \
		-r'(``@object substrings: ``@args1)->(``@object subStrings: ``@args1)' \
		-r'(Dictionary from: ``@args1)->(Dictionary newFrom: ``@args1)' \
		-r'(``@object copyFrom: ``@args1)->(``@object copyFrom: ``@args1 to: ``@object size)' \
		-r'Sockets.SocketAddress->GSTSocketAddress' \
		-r'((Osmo at: \#SIPParser) combineUri: ``@args1)->(SIPParser combineUri: ``@args1)' \
		-r'``@object nl->``@object lf' \
		-r'RecursionLock->Mutex' \
		-r'SystemExceptions.EndOfStream->Exception' \
		-r'Sockets.Datagram->OsmoUDPDatagram' \
		-r'Sockets.DatagramSocket new->Socket newUDP' \
		-r'(``@obj startsWith: ``@arg2 )->(``@obj beginsWith: ``@arg2)'  \

# Can not be parsed right now..
#		-r'(``@object => ``@args1)->(``@object ==> ``@args1)'

GRAMMAR = \
	grammar/MGCPGrammar.st \
	grammar/MGCPGrammarTest.st

CALLAGENT = \
	callagent/MGCPCallAgent.st \
	callagent/MGCPCommands.st \
	callagent/MGCPEndpoint.st \
	callagent/MGCPLogArea.st \
	callagent/MGCPParser.st \
	callagent/MGCPResponse.st \
	callagent/MGCPTransaction.st \
	callagent/MGCPTrunk.st \
	callagent/Tests.st \

#PHARO_COMPAT = pharo-porting/compat_for_pharo.st
PHARO_CHANGES = pharo-porting/changes_for_pharo.st


all:
	$(GST_PACKAGE) --test package.xml

convert:
	$(GST_CONVERT) $(CONVERT_RULES) -F squeak -f gst \
		-o fileout.st $(PHARO_COMPAT) \
		$(GRAMMAR) $(CALLAGENT) \
		$(PHARO_CHANGES)
	sed -i s,"=>","==>",g fileout.st
